''

GENERAR 100K REGISTROS CON SEXO = 1, 1K CON SEXO = 2

NOMBRE      RANDOM ALFANUM
SEXO 
PAIS        RANDOM ENTRE 1 Y 4
ID          SECUENCIAL UNICO


'''
import random
import string
from faker import Faker
from random import randint

falsillo = Faker()

sexo = 0
id = 0
nombre = ""
pais = 0


file = open("registros.txt", 'wt')
for i in range(1,101001):
    nombre = falsillo.name().split()[0] + ' ' + ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(4))
    if (i<=1000):
        sexo=2
    else:
        sexo=1
    pais = randint(1,4)
    id = i
    file.write(f'{sexo}|{nombre}|{pais}|{id}\n')
file.close()

