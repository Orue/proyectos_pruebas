irectorio_archivos='./fuente/'
archivos=$(ls $directorio_archivos)

    #-----------------------------------------------------------------------#
    #                                                                       #
    #    toma los archivos de la carpeta directorio_archivos y los itera    #
    #               creando una carpeta para cada uno.                      #
    #                                                                       #
    #-----------------------------------------------------------------------#

for archivo in $archivos
do

    nombre=$(echo $archivo | cut -d '.' -f1)
    carpeta='ng_'$nombre


    #----------------------------------------#
    #                                        #
    #    si la carpeta no existe, la crea    #
    #                                        #
    #----------------------------------------#

    if ! [ -d $carpeta ]
    then
        fecha=$(date +%x' '%X)
        mkdir -p 'files/'$carpeta
        echo $fecha" se ha creado exitosamente la carpeta "$carpeta".nbas" >> "procesos_"${0:0:-3}".txt" 2>&1
        bash ./2_generador_archivos.sh $carpeta

    fi


done
