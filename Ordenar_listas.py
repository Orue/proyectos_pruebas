
lista = [2,4,6,8,10,11,14,16]

def esMasChico(n,m):
    if (n < m):
        return True
    return False

def esMasGrande(n,m):
    if (n > m):
        return True
    return False

# si esta ordenado de forma Ascendente
def estaOrdenadoAsc(lista):
    ret = True
    for i in range (0,len(lista)-1):
        ret = ret and esMasChico(lista[i], lista[i+1])
    return  ret


print("Son todos Ascendentes: ",estaOrdenadoAsc(lista))


### si esta de forma Descendente
def estaOrdenadoDesc(lista):
    ret = True
    for i in range(0,len(lista)-1):
        ret = ret and esMasGrande(lista[i], lista[i+1])
    return  ret

print("Son todos descendente: ",estaOrdenadoDesc(lista))

# si no esta oredenado asc y no esta ordenado desc, entonces esta desordenado
def estaDesordenado():
    descendente = estaOrdenadoDesc(lista)
    ascendente = estaOrdenadoAsc(lista)
    desordenado = True
    if (descendente == False and  ascendente == False):
        return desordenado
    return False

print("la lista esta desordenada: ",estaDesordenado())
